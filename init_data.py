import pandas as pd
from sqlalchemy import create_engine

engine = create_engine('postgresql://postgres:qwerty@localhost:5432/test')
file = 'group_list.xlsx'

xl = pd.ExcelFile(file)
df = xl.parse('group_list')
df.to_sql('group_list', engine)
