## Lab 1 - part 2
***
*Работа выполнена Терентьевой Татьяной Алексеевной, БСБО-05-19*

### Краткое описание функционала
***
Проект состоит из 2 отдельных сервисов, написанных на языке программирования python:
* **Сервис с данными** - `init_group.py`:
	* данные хранятся в `.xlsx` файле
    * сервис подключается к базе данных PostgreSQL
    * сервис импортирует данные в базу данных PostgreSQL
* **Сервис приложения, которое обращается к сервису с данными и запрашивает у него некую информацию** - `app.py`:
	* сервис подключается к базе данных PostgreSQL
	* сервис выводит список студентов в виде таблицы, отсортированной по дате рождения
	* результат работы сервиса также сохраняется в виде артефакта по пути `output\output.csv`

Сборка этих сервисов производится средствами CI и docker, конфигурация указана в файлах `.gitlab-ci.yml`, `Dockerfile`, `docker-compose.yml`

### Скриншоты
***
**Пример данных (файл `group_list.py`, таблица 1):**

![img.png](./imgs/img.png)

**Пример результата работы приложения (данные из `group_list.py`):**

![img_1.png](./imgs/img_1.png)