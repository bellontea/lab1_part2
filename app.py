import psycopg2
from pandas import DataFrame

conn = psycopg2.connect(dbname='test', user='postgres', password='qwerty', host='localhost')
cursor = conn.cursor()
cursor.execute('SELECT full_name, birth_date FROM group_list')
df = DataFrame(cursor.fetchall())
res = df.sort_values(by='birth_date')
print(res)
res.to_csv('./output/output.csv')
